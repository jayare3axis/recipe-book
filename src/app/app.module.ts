import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CommonModule } from '@angular/common';

import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { MatDialogModule } from '@angular/material/dialog';

import { HomeComponent } from './views/home/home.component';
import { TitleComponent } from './views/title/title.component';
import { RecipeListComponent } from './views/recipe-list/recipe-list.component';

import { environment } from '../environments/environment';
import { ConfirmationDialogComponent } from './views/confirmation-dialog/confirmation-dialog.component';
import { RecipeEditComponent } from './views/recipe-edit/recipe-edit.component';
import { RecipeViewComponent } from './views/recipe-view/recipe-view.component';
import { FooterComponent } from './views/footer/footer.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { LoginButtonComponent } from './views/login-button/login-button.component';
import { LoginDialogComponent } from './views/login-dialog/login-dialog.component';
import { LogoutButtonComponent } from './views/logout-button/logout-button.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TitleComponent,
    RecipeListComponent,
    ConfirmationDialogComponent,
    RecipeEditComponent,
    RecipeViewComponent,
    FooterComponent,
    LoginButtonComponent,
    LoginDialogComponent,
    LogoutButtonComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MatButtonModule,
    MatIconModule,
    MatTableModule,
    MatDialogModule,
    MatInputModule,
    MatPaginatorModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFireDatabaseModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
