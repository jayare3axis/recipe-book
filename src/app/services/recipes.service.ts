import { Injectable } from '@angular/core';
import { Recipe, RecipeSnapshot } from '../model/recipe';
import { database } from 'firebase/app';
import { Observable } from 'rxjs';
import { map, first } from 'rxjs/operators';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';

const DB_RECIPES_PATH = 'recipes';

@Injectable({
  providedIn: 'root'
})
export class RecipesService {

  private recipeList = this.db.list(`/${DB_RECIPES_PATH}`);

  constructor(private db: AngularFireDatabase) {}

  getRecipeKeys(): Observable<string[]> {
    return new Observable((subscriber) => {
      this.getRecipesList()
      .pipe(first())
      .subscribe(recipes => {
        const keys = recipes.map(snapshot => snapshot.key);
        subscriber.next(keys);
        subscriber.complete();
      });
    });    
  }

  getRecipesList(pageSize = 0, startAt: string|undefined = undefined): Observable<RecipeSnapshot[]> {
    const list: AngularFireList<Recipe> = 
      this.db.list(`/${DB_RECIPES_PATH}`, ref =>
        startAt ?          
          ref.orderByKey().startAt(startAt).limitToFirst(pageSize) :
          pageSize ?
            ref.orderByKey().limitToFirst(pageSize) :
            ref.orderByKey()

    );
    return list.snapshotChanges()
      .pipe(map((changes) => {
        return changes.map(change => ({
          key: change.payload.key,
          snapshot: change.payload,
          data: {
            key: change.payload.key,
            ...change.payload.val()
          }
        }));
      }));
  }

  getRecipe(key: string): Observable<Recipe> {
    return new Observable<Recipe>(subscribe => {
      this.getRecipesList()
        .pipe(first())
        .subscribe(snapshots => {
          for (const snapshot of snapshots) {
            if (snapshot.key === key) {
              subscribe.next(snapshot.data);
              subscribe.complete();
              return;
            }
          }
        });
    });
  }

  addRecipe(recipe: Recipe): database.ThenableReference {    
    return this.recipeList.push(recipe);
  }

  removeRecipe(key: string): Promise<void> {
    return this.recipeList.remove(key);
  }

  updateRecipe(key: string, values: Partial<Recipe>): Promise<void> {
    const recipeObject = this.db.object(`/${DB_RECIPES_PATH}/${key}`);
    return recipeObject.update(values);
  }
}
