import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './views/home/home.component';
import { RecipeEditComponent } from './views/recipe-edit/recipe-edit.component';
import { RecipeViewComponent } from './views/recipe-view/recipe-view.component';

const routes: Routes = [{
  component: RecipeEditComponent,
  path: "update/:key"
}, {
  component: RecipeEditComponent,
  path: "update"
}, {
  component: RecipeViewComponent,
  path: "recipe/:key"
}, {
  component: HomeComponent, 
  path: "", 
  pathMatch: "full"
}, { 
  component: HomeComponent, 
  path: "**" 
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
