import { DataSnapshot } from '@angular/fire/database/interfaces';

export interface Recipe {    
    key?: string;
    name: string;
    text: string;
}

export interface RecipeSnapshot {
    key: string;
    snapshot: DataSnapshot;
    data: Recipe;
}
