import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { LoginDialogComponent } from '../login-dialog/login-dialog.component';

@Component({
  selector: 'app-login-button',
  templateUrl: './login-button.component.html',
  styleUrls: ['./login-button.component.css']
})
export class LoginButtonComponent {

  constructor(private dialog: MatDialog) { }

  onLogin() {
    const dialogRef = this.dialog.open(LoginDialogComponent, {
      width: '350px'
    });
  }

}
