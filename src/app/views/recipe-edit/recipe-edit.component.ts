import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Recipe } from './../../model/recipe';
import { first } from 'rxjs/operators';
import * as _ from 'lodash';
import { RecipesService } from './../../services/recipes.service';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css']
})
export class RecipeEditComponent implements OnInit {
  recipeForm = this.createForm();

  isBusy = false;

  get isNew(): boolean {
    return _.isNil(this.key);
  }

  private key?: string;

  constructor(private router: Router, private route: ActivatedRoute, private recipes: RecipesService) { }

  ngOnInit() {
    this.key = this.route.snapshot.paramMap.get('key');

    if (!this.isNew) {
      this.recipes.getRecipe(this.key)
        .pipe(first())
        .subscribe(recipe => {
          this.recipeForm = this.createForm(recipe);
        });
    }
  }

  onSubmit() {
    this.isBusy = true;

    if (this.isNew) {
      this.recipes.addRecipe(this.recipeForm.value)
        .then(() => {
          this.isBusy = false;
          this.router.navigate(['']);
        });
    }
    else {
      this.recipes.updateRecipe(this.key, this.recipeForm.value)
        .then(() => {
          this.isBusy = false;
          this.router.navigate(['']);
        });
    }
  }

  onCancelClick() {
    this.router.navigate(['']);
  }

  private createForm(recipe?: Recipe): FormGroup {
    return new FormGroup({
      name: new FormControl(recipe?.name, [Validators.required]),
      text: new FormControl(recipe?.text)
    });
  }

}
