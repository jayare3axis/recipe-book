import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { RecipesService } from './../../services/recipes.service';
import { Recipe } from './../../model/recipe';

@Component({
  selector: 'app-recipe-view',
  templateUrl: './recipe-view.component.html',
  styleUrls: ['./recipe-view.component.css']
})
export class RecipeViewComponent implements OnInit {

  recipe: Recipe;
  
  constructor(private router: Router, private route: ActivatedRoute, private recipes: RecipesService) { }

  ngOnInit() {
    const key = this.route.snapshot.paramMap.get('key');

    this.recipes.getRecipe(key)
      .pipe(first())
      .subscribe(recipe => {
        this.recipe = recipe;
      });
  }

}
