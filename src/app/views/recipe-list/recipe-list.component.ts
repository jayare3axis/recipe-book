import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { first } from 'rxjs/operators';
import { RecipesService } from './../../services/recipes.service';
import { Recipe, RecipeSnapshot } from './../../model/recipe';
import { ConfirmationDialogComponent } from './../../views/confirmation-dialog/confirmation-dialog.component';
import { PageEvent } from '@angular/material/paginator';
import * as _ from 'lodash';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit, OnDestroy {

  data: Recipe[];

  get displayedColumns() {
    const result = ['name'];
    this.auth.isLoggedIn && result.push('edit');
    return result;
  }

  recipesKeys: string[];  
  recipesCount: number;

  recipeListSize = 10;
  pageSizeOptions = [5, 10, 25, 100];

  private destroy$ = new Subject();

  constructor(public auth: AuthService, private recipes: RecipesService, private dialog: MatDialog, private router: Router) {}

  ngOnInit() {
    this.recipes.getRecipeKeys()
      .pipe(first())
      .subscribe(keys => {
        this.recipesKeys = keys;
        this.recipesCount = keys.length;
      });
    this.getRecipes(this.recipeListSize);    
  }

  ngOnDestroy() {
    this.destroy$.next();
  }

  onAddRecipe() {
    this.router.navigate(['/update']);
  }

  onEdit(key: string) {
    this.router.navigate(['/update', key]);
  }

  onRemove(name: string, key: string) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: {
        title: 'Delete for all eternity?',
        message: `Delete the recipe "${name}"?`
      }
    });

    dialogRef.afterClosed()
      .pipe(first())
      .subscribe(result => {
        if (!result) {
          return;
        }

        this.recipes.removeRecipe(key).then(() => {
          this.getRecipes(this.recipeListSize);
        });
      });
  }

  onPage(event: PageEvent) {
      const startAt = this.recipesKeys[event.pageIndex * event.pageSize];
      (event.pageIndex === 0) ?
        this.getRecipes(event.pageSize) :    
        this.getRecipes(event.pageSize, startAt);
  }

  private getRecipes(pageSize: number, startAt: string|undefined = undefined) {
    this.recipes.getRecipesList(pageSize, startAt)
    .pipe(first())
    .subscribe((recipes: RecipeSnapshot[]) => {
      this.data = recipes.map(snapshot => snapshot.data);
    });
  }

}
