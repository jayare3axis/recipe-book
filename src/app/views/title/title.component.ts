import { Component, Input, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';

const EASTER_EGG = ['Made', 'by', 'Tatiana', 'and', 'Jindřich'];

@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.css']
})
export class TitleComponent implements OnInit {

  @Input()
  value = '';

  parsedValue = '';

  constructor(public auth: AuthService) {}

  ngOnInit() {
    this.parsedValue = this.value;
  }

  onDblClick(event:MouseEvent) {
    this.timer ? this.stopEasterEgg() : this.runEasterEgg();
  }

  private timer:any;

  private runEasterEgg() {
    let count = 0;

    this.timer = setInterval(() => {
      count = ++count % 6;

      this.parsedValue = (count === 0) ? this.value : EASTER_EGG[count - 1];
    }, 1000);
  }

  private stopEasterEgg() {
    clearTimeout(this.timer);
    this.timer = null;
    this.parsedValue = this.value;
  }

}
