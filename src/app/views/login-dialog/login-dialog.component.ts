import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { User } from 'firebase';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { UserCredential } from './../../model/user';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-login-dialog',
  templateUrl: './login-dialog.component.html',
  styleUrls: ['./login-dialog.component.css']
})
export class LoginDialogComponent implements OnInit, OnDestroy {

  errorMessage = "";

  userForm = new FormGroup({
    email: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  });

  user: User;

  private destroy$ = new Subject<void>();

  constructor(public dialogRef: MatDialogRef<LoginDialogComponent>, public auth: AuthService, public dialog: MatDialog) {}

  ngOnInit() {
    this.auth.fireAuth.authState
      .pipe(takeUntil(this.destroy$))
      .subscribe(user => {
        if (user) {
          this.user = user;
          localStorage.setItem('user', JSON.stringify(this.user));
        } 
        else {
          localStorage.setItem('user', null);
        }
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
  }
    
  onNoClick() {
    this.dialogRef.close();
  }

  onLoginClick() {
    const userCredential: UserCredential = this.userForm.value;
    this.auth.login(userCredential.email, userCredential.password)
      .then(result => {
        this.errorMessage = "";
        this.dialogRef.close();
      })    
      .catch(error => {
        this.errorMessage = error.message;
      })
  }

}
