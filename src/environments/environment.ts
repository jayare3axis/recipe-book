// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false, 
  firebase: {
    apiKey: "AIzaSyByqGzjgHlsJIRPtWmwdEKV8Jthsrjvu2M",
    authDomain: "recipe-book-fdcaf.firebaseapp.com",
    databaseURL: "https://recipe-book-fdcaf.firebaseio.com",
    projectId: "recipe-book-fdcaf",
    storageBucket: "recipe-book-fdcaf.appspot.com",
    messagingSenderId: "198715313606",
    appId: "1:198715313606:web:099009b18ee5054261f6c7"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
